$(document).ready ->
  menu = $(".trigger")
  menuToggle = $(".menu-icon")
  $(menuToggle).on "click", (e) ->
    e.preventDefault()
    menu.toggle()
    return

  $(".nav .nav-link").click ->
    $(".nav .nav-link").each ->
      $(this).removeClass "active-nav-item"
      return

    $(this).addClass "active-nav-item"
    $(".nav .more").removeClass "active-nav-item"
    return

  return
